#!/usr/bin/env pwsh

function Find-LongestWord{
    [OutputType([string])]
    Param(
        [Parameter(ValueFromPipeline, ParameterSetName='AsPipeline',Mandatory=$true, Position=1)]
        [string] $Word,

        # An array of words to examine
        [Parameter(ParameterSetName='AsArray', Mandatory=$true, Position=1)]
        [string[]] $Words
    )


    Begin {
        if ($Words) {
            $longest = $Words | Find-LongestWord
        } else {
            $longest = $null
        }
    }

    Process {
        if ($Word -and $Word.Length -gt $longest.Length) {
            $longest = $Word
        }
    }

    End {
        return $longest   
    }
}
