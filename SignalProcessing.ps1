#!/usr/bin/env pwsh

# Get the text of the declaration of independence
$declarationText = (Invoke-WebRequest "http://www.constitution.org/usdeclar.txt").Content

# split the words
$words = [Regex]::split($declarationText, "\s+")
$words.Length

# Remove empties
$words = $words | Where-Object {($_)}

# Remove punctuation
$words = $words | ForEach-Object -Process { $_.TrimEnd((',', '.', ';'))}

# Exclude the articles
$importantWords = $words | Where-Object {$_ -ne "a" -and $_ -ne "an" -and $_ -ne "the"}
$importantWords.Length

# Find the longest word
$words | ForEach-Object -Begin {$longest = ""} `
    -Process {if ($_.Length -gt $longest.Length) {$longest = $_} } `
    -End {$longest}

# Get a map of word counts
$words | % -Begin {$wordCounts = @{} } `
    -Process {$wordCounts[$_.ToLower()]++} 

# Export counts as csv
$wordCounts.keys | % { 
    [pscustomobject]@{word = $_; count = $wordCounts[$_] }
} | Sort-Object -Property count | Export-Csv /var/tmp/counts.csv
